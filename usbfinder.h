#ifndef USBFINDER_H
#define USBFINDER_H

#include <QWidget>
#include <QtGui>
#include <QAndroidJniObject>

QT_BEGIN_NAMESPACE
namespace Ui {
class USBFinder;
}
//class QPushButton;
class QTextEdit;
QT_BEGIN_NAMESPACE

class USBFinder : public QWidget
{
    Q_OBJECT

public:
    explicit USBFinder(QWidget *parent = 0);
    ~USBFinder();

private slots:
    void on_pushButton_clicked();

private:
    Ui::USBFinder *ui;

//    QPushButton *ui_button;
    QTextEdit *ui_text;

private:
    QString getUsbDeviceList();
    QString libUsbDevList();
    QString libRtlSrdDevList();
};

#endif // USBFINDER_H
