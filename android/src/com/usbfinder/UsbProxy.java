package com.usbfinder;

import java.util.HashMap;
import java.util.Iterator;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

public class UsbProxy extends org.qtproject.qt5.android.bindings.QtActivity {

    private static UsbProxy m_instance;
    private static UsbManager usbManager;

    public UsbProxy() {
        m_instance = this;
    }

    public static String getUsbDeviceList(String str)
//    public static String getUsbDeviceList()
    {
        String text = "";

        usbManager = (UsbManager) m_instance.getSystemService(Context.USB_SERVICE);

        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        while(deviceIterator.hasNext()){
            UsbDevice device = deviceIterator.next();
            text += device.getDeviceName();
        }

//        text = usbManager.toString() + str;

        return text + str;
    }
}

