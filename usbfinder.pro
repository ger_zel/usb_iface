#-------------------------------------------------
#
# Project created by QtCreator 2015-03-01T19:35:36
#
#-------------------------------------------------

QT       += core androidextras

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = usbfinder
TEMPLATE = app


SOURCES += main.cpp\
        usbfinder.cpp

OTHER_FILES += \
    android/src/com/usbfinder/UsbProxy.java

HEADERS  += usbfinder.h

FORMS    += usbfinder.ui

CONFIG += mobility
MOBILITY = 

LIBS += -L/media/Home/libs/libusb/jni/usb -lusb
INCLUDEPATH += /media/Home/libs/libusb/jni/usb

LIBS += -L/media/Home/libs/librtl/jni -lrtlsdr
INCLUDEPATH += /media/Home/libs/librtl/jni/include
INCLUDEPATH += /media/Home/libs/librtl/jni/src/convenience
INCLUDEPATH += /media/Home/libs/librtl/jni/src/getopt

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew
