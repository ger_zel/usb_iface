#include "usbfinder.h"
#include "ui_usbfinder.h"
#include <libusb.h>
#include <cassert>
#include <rtl-sdr.h>
#include <convenience.h>

USBFinder::USBFinder(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::USBFinder)
{
    ui->setupUi(this);

    ui_text = findChild<QTextEdit*>("textEdit");

    QMetaObject::connectSlotsByName(this);
}

USBFinder::~USBFinder()
{
    delete ui;
}

void USBFinder::on_pushButton_clicked()
{
    QString text = "usb list:";
//    QString list = getUsbDeviceList();
//    QString list = libUsbDevList();
    QString list = libRtlSrdDevList();

    ui_text->clear();

    ui_text->insertPlainText(text + list);
}

QString USBFinder::getUsbDeviceList()
{
    QAndroidJniObject val = QAndroidJniObject::fromString("");

    QAndroidJniObject str = QAndroidJniObject::callStaticObjectMethod(
              "com/usbfinder/UsbProxy"
              ,"getUsbDeviceList"
              ,"(Ljava/lang/String;)Ljava/lang/String;"
              ,val.object<jstring>());

//    QAndroidJniObject str = QAndroidJniObject::callStaticObjectMethod(
//              "com/usbfinder/UsbProxy"
//              ,"getUsbDeviceList"
//              ,"Ljava/lang/String;");

    QString qstring = str.toString() + val.toString();
//    QString qstring = str.toString();

    return qstring;
}

QString USBFinder::libUsbDevList()
{
    libusb_context *context = NULL;
    libusb_device **list = NULL;
    int ret = 0;
    ssize_t count = 0;
    QString devices = "";

    ret = libusb_init(&context);
    assert(ret == 0);

    count = libusb_get_device_list(context, &list);
//    assert(count > 0);

    for (size_t idx = 0; idx < count; ++idx) {
        libusb_device *device = list[idx];
        libusb_device_descriptor desc = {0};

        ret = libusb_get_device_descriptor(device, &desc);
        assert(ret == 0);

//        printf("Vendor:Device = %04x:%04x\n", desc.idVendor, desc.idProduct);

        devices += "Device: " + QString::number(desc.idProduct) + ":" + QString::number(desc.idProduct);
    }

    return devices;
}

QString USBFinder::libRtlSrdDevList()
{
    QString devices = "";

//    int device_index = verbose_device_search("0");
    int device_index = rtlsdr_get_device_count();

    devices = "Devices num: " + QString::number(device_index);

    return devices;
}
